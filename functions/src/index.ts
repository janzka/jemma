import * as functions from 'firebase-functions';

import admin = require('firebase-admin');
import express = require('express');
import cors = require('cors');

admin.initializeApp();

const app = express();
const db = admin.firestore();

// Allow cross-origin requests anywhere on emulator
if (process.env.FUNCTIONS_EMULATOR) app.use(cors({ origin: true }));
// And from "official" URL in production
else app.use(cors({ origin: 'https://jemma-link.web.app' }));

// Get secret
app.get('/:id', async (req, res) => {
  const ref = db.collection('secrets').doc(req.params.id);

  const secret = await db.runTransaction<void | Buffer>((transaction) => transaction.get(ref).then((doc) => {
    if (!doc.exists) return;

    transaction.delete(ref);
    return doc.data()?.d;
  }));

  if (!secret) res.statusCode = 404;
  res.send(secret);
});

// Expose secrets API
export const secrets = functions.region('europe-west1').runWith({ memory: '128MB', timeoutSeconds: 10 }).https.onRequest(app);
