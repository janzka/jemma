import 'dart:typed_data';

import 'package:http/http.dart' as http;

class CloudFunctions {
  static String host = 'https://europe-west1-jemma-link.cloudfunctions.net';
  static final _client = http.Client();

  static Future<Uint8List> getSecret(String id) async {
    final response = await _client.get(Uri.parse('$host/secrets/$id'));
    return response.bodyBytes;
  }
}