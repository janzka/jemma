@JS('sodium')
library sodium;

import 'dart:typed_data';

import 'package:js/js.dart';

@JS('crypto_generichash')
external Uint8List cryptoGenerichash(int bytes, Uint8List data);

@JS('crypto_secretbox_easy')
external Uint8List cryptoSecretboxEasy(Uint8List message, Uint8List nonce, Uint8List key);

@JS('crypto_secretbox_NONCEBYTES')
external int get cryptoSecretboxNONCEBYTES;

@JS('crypto_secretbox_open_easy')
external Uint8List cryptoSecretboxOpenEasy(Uint8List cipherText, Uint8List nonce, Uint8List key);

@JS('crypto_secretbox_keygen')
external Uint8List cryptoSecretboxKeygen();

@JS('randombytes_buf')
external Uint8List randombytesBuf(int size);