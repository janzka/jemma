import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'helpers/cloud_functions.dart';
import 'secrets/new_secret_screen.dart';
import 'secrets/open_secret_screen.dart';
import 'secrets/secret_arguments.dart';

class App extends StatelessWidget {

  App() {
    // Setup environment for local development
    if(kDebugMode) {
      FirebaseFirestore.instance.settings = Settings(
        host: "localhost:8080",
        sslEnabled: false,
        persistenceEnabled: false,
      );

      CloudFunctions.host = 'http://localhost:5001/jemma-link/europe-west1';
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Jemma',
      onGenerateInitialRoutes: _onGenerateInitialRoutes,
      onGenerateRoute: _onGenerateRoute,
      theme: ThemeData.light(),
      darkTheme: ThemeData.dark(),
      themeMode: ThemeMode.system
    );
  }

  List<MaterialPageRoute> _onGenerateInitialRoutes(String path) {
    return [ _onGenerateRoute(RouteSettings(name: path)) ];
  }

  MaterialPageRoute _onGenerateRoute(RouteSettings settings) {
    final uri = Uri.parse(settings.name);

    if(uri.pathSegments.length > 0 && uri.pathSegments[0] == 'secret') {
      final id = uri.pathSegments.length > 1 ? uri.pathSegments[1] : null;
      final nonce = uri.pathSegments.length > 2 ? base64.decode(Uri.decodeComponent(uri.pathSegments[2])) : null;
      final key = uri.pathSegments.length > 3 ? base64.decode(Uri.decodeComponent(uri.pathSegments[3])) : null;

      final arguments = SecretArguments(id, nonce, key);

      return MaterialPageRoute(builder: (_) => OpenSecretScreen(arguments));
    }

    return MaterialPageRoute(builder: (_) => NewSecretScreen(), settings: RouteSettings(name: '/'));
  }
}