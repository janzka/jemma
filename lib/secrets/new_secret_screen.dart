import 'package:flutter/material.dart';

import 'new_secret_form.dart';
import 'new_secret_link.dart';
import 'secret_arguments.dart';

class NewSecretScreen extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<NewSecretScreen> {
  SecretArguments secret;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          constraints: const BoxConstraints(maxWidth: 512),
          padding: const EdgeInsets.all(16),
          child: secret != null ? NewSecretLink(secret: secret) : NewSecretForm(onSecretCreated: (secret) => setState(() => this.secret = secret))
        ),
      )
    );
  }
}