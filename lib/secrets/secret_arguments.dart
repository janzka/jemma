import 'dart:typed_data';

class SecretArguments {
  final String id;
  final Uint8List nonce;
  final Uint8List key;

  SecretArguments(this.id, this.nonce, this.key);
}