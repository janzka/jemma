import 'dart:convert';

import 'package:flutter/material.dart';

import '../helpers/cloud_functions.dart';
import '../helpers/sodium.dart' as sodium;
import 'secret_arguments.dart';

class OpenSecretScreen extends StatefulWidget {
  final SecretArguments arguments;

  const OpenSecretScreen(this.arguments, {Key key}) : super(key: key);

  @override
  _State createState() => _State();
}

class _State extends State<OpenSecretScreen> {
  bool _loading = false;
  bool _notFound = false;
  String _message;
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _body()
    );
  }

  Widget _body() {
    if(_loading) return const Center(child: CircularProgressIndicator());

    final childs = <Widget>[];

    if(_notFound) childs.add(Text('Secret does not exist. It has never existed or someone has already opened it.', style: Theme.of(context).textTheme.headline6));
    else if(_message != null) childs.add(_messageBox());
    else childs.add(_openButton());

    if(_notFound == false) childs.add(const Text('Secret can be only opened once!'));

    return Center(
      child: Container(
        constraints: const BoxConstraints(maxWidth: 512),
        padding: const EdgeInsets.all(16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: childs
        )
      )
    );
  }

  Widget _openButton() {
    return Container(
      margin: EdgeInsets.only(bottom: 16 + Theme.of(context).visualDensity.vertical),
      width: double.infinity,
      child: OutlinedButton(
        child: Text('Open secret'),
        onPressed: _openSecret
      )
    );
  }

  Widget _messageBox() {
    return Container(
      margin: EdgeInsets.only(bottom: 16 + Theme.of(context).visualDensity.vertical),
      child: TextFormField(
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: 'Secret message',
        ),
        maxLines: null,
        readOnly: true,
        initialValue: _message,
      ),
    );
  }

  _openSecret() async {
    setState(() {
      _loading = true;
    });

    final secret = await CloudFunctions.getSecret(widget.arguments.id);
    if(secret.isNotEmpty) {
      _message = utf8.decode(sodium.cryptoSecretboxOpenEasy(secret, widget.arguments.nonce, widget.arguments.key));
    } else {
      _notFound = true;
    }

    setState(() {
      _loading = false;
    });
  }
}