import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'secret_arguments.dart';

class NewSecretLink extends StatelessWidget {
  final SecretArguments secret;
  final TextEditingController _controller = TextEditingController();

  NewSecretLink({Key key, this.secret}) : super(key: key) {
    final uri = '${Uri.base.origin}/#/secret/${secret.id}/${Uri.encodeComponent(base64.encode(secret.nonce))}/${Uri.encodeComponent(base64.encode(secret.key))}';
    _controller.text = uri;
    _controller.selection = TextSelection(baseOffset: 0, extentOffset: _controller.value.text.length);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        _linkBox(context),
        _deleteButton(context),
        _createAnotherButton(context)
      ],
    );
  }

  Widget _linkBox(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 16 + Theme.of(context).visualDensity.vertical),
      child: TextFormField(
        autofocus: true,
        controller: _controller,
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: 'Share this link',
          suffixIcon: Tooltip(
            child: IconButton(
              icon: Icon(Icons.copy),
              onPressed: () {
                Clipboard.setData(ClipboardData(text: _controller.text));
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Link copied to clipboard')));
              } 
            ),
            message: 'Copy link to clipboard',
          ),
        ),
        maxLines: null,
        readOnly: true,
        onTap: () => _controller.selection = TextSelection(baseOffset: 0, extentOffset: _controller.value.text.length),
      ),
    );
  }

  Widget _deleteButton(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 16 + Theme.of(context).visualDensity.vertical),
      width: double.infinity,
      child: ElevatedButton(
        child: Text('Delete secret'),
        onPressed: () async {
          await FirebaseFirestore.instance.collection('secrets').doc(secret.id).delete();
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Secret deleted')));
          Navigator.of(context).pushNamedAndRemoveUntil('/', (route) => false);
        },
        style: ElevatedButton.styleFrom(
          primary: Colors.redAccent
        ),
      ),
    );
  }

  Widget _createAnotherButton(BuildContext context) {
    return Container(
      width: double.infinity,
      child: OutlinedButton(
        child: Text('Create another secret'),
        onPressed: () => Navigator.of(context).pushReplacementNamed('/'),
      )
    );
  }
}