import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../helpers/sodium.dart' as sodium;
import '../components/loading_widget.dart';
import 'secret_arguments.dart';

class NewSecretForm extends StatefulWidget {
  final Function(SecretArguments secret) onSecretCreated;

  const NewSecretForm({Key key, this.onSecretCreated}) : super(key: key);

  @override
  _State createState() => _State();
}

class _State extends State<NewSecretForm> {
  bool _loading = false;
  String _message = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _body()
      )
    );
  }

  Widget _body() {
    if(_loading) return const LoadingWidget();

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        _messageBox(),
        _createLinkButton()
      ],
    );
  }

  Widget _messageBox() {
    return Container(
      margin: EdgeInsets.only(bottom: 16 + Theme.of(context).visualDensity.vertical),
      child: TextFormField(
        autofocus: true,
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          hintText: 'Write your secret here and press create link'
        ),
        minLines: 5,
        maxLines: null,
        onChanged: _onMessageChanged,
      )
    );
  }

  Widget _createLinkButton() {
    return Container(
      width: double.infinity,
      child: OutlinedButton(
        child: Text('Create link'),
        onPressed: _message.isNotEmpty ? _createLink : null
      )
    );
  }

  _onMessageChanged(String message) {
    setState(() {
      _message = message;
    });
  }

  _createLink() async {
    setState(() {
      _loading = true;
    });

    final nonce = sodium.randombytesBuf(sodium.cryptoSecretboxNONCEBYTES);
    final key = sodium.cryptoSecretboxKeygen();
    final encrypted = sodium.cryptoSecretboxEasy(utf8.encode(_message), nonce, key);

    try {
      final secret = await FirebaseFirestore.instance.collection('secrets').add({'d': Blob(encrypted)});

      final arguments = SecretArguments(secret.id, nonce, key);
      widget.onSecretCreated?.call(arguments);
    } catch(e) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Something went wrong :(.')));
      
      print('Error!');
      print(e);

      setState(() {
        _loading = false;
      });
    }
  }
}