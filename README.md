# Jemma

Jemma is really e2e encrypted communication web application.

## Getting Started

- First install required and optional tools
- If using VSC then Run > Start Debugging (F5) should work
- If not using VSC then it's up to you to figure out how to start application locally

### Required tools
- Flutter https://flutter.dev/docs/get-started/install
    - See version from [pubspec.yaml](./pubspec.yaml)
- Node.js https://nodejs.org/en/
    - See version from functions [package.json](./functions/package.json)
- Firebase CLI https://firebase.google.com/docs/cli

### Optional tools
- Visual Studio Code https://code.visualstudio.com/
    - Flutter Intl extension https://marketplace.visualstudio.com/items?itemName=localizely.flutter-intl